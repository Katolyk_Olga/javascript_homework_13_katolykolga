// Теоретичні питання
// 1. Опишіть своїми словами різницю між функціями setTimeout() і setInterval().
// setTimeout() - запучскає одноразово функцію через певний вказаний час, 
// а setInterval() - запускає повторно функцію багато разів з вказаним часовим інтервалом

// 2. Що станеться, якщо в функцію setTimeout() передати нульову затримку? Чи спрацює вона миттєво і чому?
// При встановленні нульової затримки (0 мілісекунд) виклик буде відкладений у чергу викликів та почекає, поки виконується основний код.

// 3. Чому важливо не забувати викликати функцію clearInterval(), коли раніше створений цикл запуску вам вже не потрібен?
// Тому що він буде виконуватися безкінечно, і це марна  витрата ресурсів.


// Завдання
// Реалізувати програму, яка циклічно показує різні картинки. 
// Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.

// Технічні вимоги:
// У папці banners лежить HTML код та папка з картинками.
// + При запуску програми на екрані має відображатись перша картинка.
// + Через 3 секунди замість неї має бути показано друга картинка.
// + Ще через 3 секунди – третя.
// + Ще через 3 секунди – четверта.
// + Після того, як будуть показані всі картинки - цей цикл має розпочатися наново.
// + Після запуску програми десь на екрані має з"явитись кнопка з написом Припинити.
// + Після натискання на кнопку Припинити цикл завершується, на екрані залишається показаною та картинка, 
// яка була там при натисканні кнопки.
// + Поруч із кнопкою Припинити має бути кнопка Відновити показ, при натисканні якої цикл триває з тієї картинки, 
// яка в даний момент показана на екрані.
// + Розмітку можна змінювати, додавати потрібні класи, ID, атрибути, теги.

// Необов"язкове завдання підвищеної складності
// - При запуску програми на екрані повинен бути таймер з секундами та мілісекундами, що показує, 
// скільки залишилося до показу наступної картинки.
// - Робити приховування картинки та показ нової картинки поступовим (анімація fadeOut/fadeIn) протягом 0.5 секунди.


let images = document.querySelectorAll(".image-to-show");
console.log(images);

// let imagesWrapper = document.querySelector(".images-wrapper");
// console.log(imagesWrapper);

// images.forEach((image) => image.style.display = "none");
images.forEach((image) => image.hidden = true);
images[0].hidden = false;
console.log(images[0]);

const btnStop = document.createElement("button");
btnStop.textContent = "Припинити";
btnStop.classList = "buttom";

const btnContinue = document.createElement("button");
btnContinue.textContent = "Відновити показ";
btnContinue.classList = "buttom";

document.body.append(btnStop, btnContinue);

btnStop.style.cssText = "display:none; justify-content:center; align-items:center; width:100px; height:60px; font-size:16px; font-weight:600";
btnStop.style.border = "3px solid purple";
btnStop.style.borderRadius = "6px";
btnStop.style.position = "absolute";
btnStop.style.left = "500px";
btnStop.style.bottom = "600px";

btnContinue.style.cssText = "display:none; justify-content:center; align-items:center; width:100px; height:60px; font-size:16px; font-weight:600";
btnContinue.style.border = "3px solid purple";
btnContinue.style.borderRadius = "6px";
btnContinue.style.position = "absolute";
btnContinue.style.left = "500px";
btnContinue.style.bottom = "500px";

setTimeout(() => { 
    btnStop.style.display = "flex";
    btnContinue.style.display = "flex";
}, 1000);

let timerId;
let showOneNextImage;

document.addEventListener("DOMContentLoaded", function () {

    // Додамо в змінну індекс поточної картинки, але почнемо відлік Індексів з 1 (не з 0), 
    // тому що 0-а картинка перед функцією вже має видимість на сторінці одразу після завантаження сторінки (DOM)
    let currentIndex = 1;
    console.log(currentIndex);

    // Функція буде показувати по одній картинці за рахунок зміни видимості елементу і зміни індексу на 1
    showOneNextImage = function showOneNextImage() {

        // Прибираємо видимість всіх картинок
        images.forEach(function (image) {
            // image.style.display = "none";
            image.hidden = true;
        });

        // Показуємо поточну картинку на сторінці
        // images[currentIndex].style.display = "block";
        images[currentIndex].hidden = false;

        // Збільшуємо індекс на 1, щоб йти на наступний елемент при наступній ітерації функції
        // Присвоюємо в змінну "поточний індекс" цілочисельну остачу, яка не розділилась на довжину масиву
        // Довжина масиву - це кількість картинок мінус 1, бо рахунок індексів в масиві починається з 0
        // Коли номер індексу буде дорівнювати довжині масиву - він розділиться без остатку, тобто нерозділена остача буде 0 
        // і цикл почнеться з початку, з першого малюнку (індекс 0)
        currentIndex = (currentIndex + 1) % images.length;
        console.log(currentIndex);
    }

    // Початковий запуск та інтервал
    timerId = setInterval(showOneNextImage, 3000);
});

btnStop.addEventListener("click", function StopShow(event) {
    console.log(event);
    clearInterval (timerId);
})

btnContinue.addEventListener("click", function ContinueShou(event) {
    console.log(event);
    timerId = setInterval(showOneNextImage, 3000);
})


// Довідка для себе:
// арифметичний оператор % повертає цілочисельну остачу, тобто число (частина діленого) яке не розділилося на дільник без остатку.
// Приклади:
// 2 % 4 = 2 (2 не змогло розділитися без остатка, тому це остача від ділення)
// 3 % 4 = 3
// 5 % 4 = 1
// 9 % 4 = 1
// 11 % 4 = 3
// 18 % 4 = 2
// 20 % 4 = 0
// 48 % 4 = 0